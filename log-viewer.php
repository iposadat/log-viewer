<?php

// Uncomment out for Debugging tools
// require './debughelper.php';

$action = isset($_POST['action']) ? $_POST['action'] : null;
$sitename = isset($_POST['sitename']) ? $_POST['sitename'] : null;
$day = isset($_POST['day'])? $_POST['day']: null;
$size = isset($_POST['size']) ? $_POST['size'] : null;
$from = isset($_POST['from']) ? $_POST['from'] : null;
$sort = isset($_POST['sort']) ? $_POST['sort'] : null;

// This fails, check it out.
// $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
// $sitename = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
// $day = filter_input(INPUT_POST, 'day', FILTER_SANITIZE_STRING);
// $size = filter_input(INPUT_POST, 'size', FILTER_SANITIZE_STRING);
// $from = filter_input(INPUT_POST, 'from', FILTER_SANITIZE_STRING);
// $sort = filter_input(INPUT_POST, 'sort', FILTER_SANITIZE_STRING);

$credentials = getenv('CREDENTIALS');
$urlESBase = getenv('ES_URL_BASE');
$indexBase = getenv('ES_INDEX_BASE');

if ($sitename) {
  $result = GetLogs($action, $day, $size, $from, $sort, $sitename);
  echo json_encode(array('status' => 'OK', 'message' => $result));
}
else {
  echo json_encode(array('status' => 'ERROR', 'message' => 'Missing property: sitename.'));
}

function GetLogs($action, $day, $size, $from, $sort, $sitename) {
		$url = BuildEsApiUrl($action, $siteType, $day);
		#debug_to_console($url,"URL");
		$body = BuildEsApiBody($size, $from, $sort, $sitename);
		#debug_to_console($body,"Body");
    $result = CallAPI('POST', $url, $body);
		
    return $result;
}

function BuildEsApiUrl($action, $day) {
	global $urlESBase, $indexBase, $day;

	// Query different indexes depending on the parameters
	$urlES_WebEOS_access = $urlESBase . $indexBase;

	switch($action) {
	  case 'getlogaccess':
			$url = $urlES_WebEOS_access . '-' . $day . '/_search';
		break;
		case 'getlogerror':
		  $url = $urlES_WebEOS_error . '-' . $day .'/_search';
		break;
	}

	return $url;
}

function BuildEsApiBody($size, $from, $sort, $sitename) {
    $body = array();
    $body["from"] = $from;
    $body["size"] = $size;
    $body["sort"]["metadata.timestamp"] = $sort;
	
	  # Further info: https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html
		$body["query"]["query_string"]["query"] = "data.sitename:" . $sitename . ".web.cern.ch";

    return $body;
}

function CallAPI($verb, $url, $data = null) {
	global $credentials;
	$ch = curl_init();

	$headers = array(
			'Content-Type: application/json'
	);

	if (($verb === 'PUT') || ($verb === 'POST')) {
			$dataStr = json_encode($data);
			$headers[] = 'Content-Length: ' . strlen($dataStr);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $verb);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);
	}

	#curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, $credentials);

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	
	$result = curl_exec($ch);

	curl_close($ch);

	return $result;
}

// function IsUserAllowed($details) {
// 	global  $username, $usergroups;

// 	$owner = $details->OwnerLogin;
// 	$allowed = $details->SiteModerators->string;
	
// 	// If only one moderator in the allowed list, convert the string into array for better handling
// 	if(!is_array($allowed)) {
// 		$allowed = array(
// 			0 => $allowed
// 		);
// 	}
	
// 	// Add the site owner to the allowed list
// 	array_push($allowed, $owner);
	
// 	// Add poweradmin groups to the allowed list
// 	array_push($allowed, "NICE WEB ADMINISTRATORS");
// 	array_push($allowed, "SERCO-ADMUS-ACCESS");
// 	array_push($allowed, "CERT-SEC");
	
// 	// If user in session is in the allowed list, return true
// 	if (in_array(strtoupper($username), $allowed)) {
// 		return true;
// 	}
// 	// Or if user in session is member of any group in the allowed list
// 	else {
// 		$groups = explode(';', $usergroups);
// 		foreach ($groups as $group) {
// 			if (in_array(strtoupper($group), $allowed)) {
// 				return true;
// 			}
// 		}
// 		return false;
// 	}
// }

?>