FROM php:7.3-stretch

RUN mkdir -p /home/log-viewer

WORKDIR /home/log-viewer

COPY . .

CMD ["php","-S","0.0.0.0:8080"]