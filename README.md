# webEOS site log viewer



## Debug the solution

```bash
# Mount the folder where the code is
docker run -it --rm -p 8080:8080 --volume //d/my-folder/log-viewer:/home/log-viewer -w /home/log-viewer php:7.3-stretch /bin/bash

# We can opt to run it in background so we can easily interact with the server at the same time.
root@7eafe2243cf3:/home/log-viewer# php -S 0.0.0.0:8080 &

# Open the browser at localhost:8080 and start debugging!
```
